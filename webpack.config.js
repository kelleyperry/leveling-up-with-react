var webpack = require('webpack');
var precss = require('precss');
var autoprefixer = require('autoprefixer');
var path = require('path');

module.exports = {
    entry: ["webpack-dev-server/client?http://localhost:8080/",
	    "webpack/hot/dev-server",
	    "./app/app.jsx"],
    output: {
	publicPath: "/js/",
	path: path.join(__dirname + "public"),
	filename: "bundle.js",
	sourceMapFilename: "bundle.map"
    },
    devtool: '#source-map',
    module: {
	loaders: [
	    {
		loader: 'babel',
		include: [path.join(__dirname, 'components'),
			  path.join(__dirname, 'app')],
		exclude: /node_modules/
	    },
	    {
		test: /\.scss$/,
		loaders: [
		    'style?sourceMap',
		    'css?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:7]',
		    'sass?sourceMap'
		]
	    }
	]
    },
    sassLoader: {
				data: '@import "' + path.resolve(__dirname, 'public/css/_settings.scss') + '";'
    },
    plugins: [],
    postcss: function() {
	return [precss, autoprofixer]
    }
}
