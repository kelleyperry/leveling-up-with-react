import React from 'react';
import { Router, Route, Link } from 'react-router';
import CSSModules from 'react-css-modules';
import styles from '../public/css/styles.scss';

class MainLayout extends React.Component {
    render () {
	return <div className="app">
				   <aside styleName="primary-aside">
		        <ul>
			       <li><Link to="/">Home</Link></li>
			       <li><Link to="/users">Users</Link></li>
			       <li><Link to="/widgets">Widgets</Link></li>
		       </ul>
		      </aside>
		      <main>
		       {this.props.children}
		      </main>
	      </div>
    }
}

export default CSSModules(MainLayout, styles);