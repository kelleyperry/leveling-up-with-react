var webpack = require("webpack");
var webpackDevServer = require("webpack-dev-server");
var config = require("./webpack.config.js");
var path = require("path");
config.plugins.push(new webpack.HotModuleReplacementPlugin());
config.entry.unshift(
    "webpack-dev-server/client?http://localhost:8080/",
    "webpack/hot/dev-server"
);

var compiler = webpack(config);
var server = new webpackDevServer(compiler, {
    contentBase: "/Users/kelleyperry/Projects/leveling-up-with-react",
    hot: true,
    quiet: true,
    noInfo: false,
    lazy: false,
    path: path.join(__dirname + "public"),
    publicPath: "/js/",
    filename: "bundle.js"

});
server.listen(8080, "localhost", function() {});

